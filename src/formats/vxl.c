/* Goxel 3D voxels editor
*
* copyright (c) 2016 Guillaume Chereau <guillaume@noctua-software.com>
*
* Goxel is free software: you can redistribute it and/or modify it under the
* terms of the GNU General Public License as published by the Free Software
* Foundation, either version 3 of the License, or (at your option) any later
* version.

* Goxel is distributed in the hope that it will be useful, but WITHOUT ANY
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
* FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
* details.

* You should have received a copy of the GNU General Public License along with
* goxel.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "goxel.h"

#define READ(type, file) \
    ({ type v; size_t r = fread(&v, sizeof(v), 1, file); (void)r; v;})
#define WRITE(type, v, file) \
    ({ type v_ = v; fwrite(&v_, sizeof(v_), 1, file);})


//TODO: decrease amount of structs and don't save junk values
typedef struct {
    char fileType[16];
    uint32_t paletteCount;
    uint32_t headerCount;
    uint32_t tailerCount;
    uint32_t bodySize;
} header_t;

typedef struct {
    uint8_t startPaletteRemap;
    uint8_t endPaletteRemap;
    uint8_t palette[256][3]; 
} vxl_palette_t;

typedef struct {
    char name[16];
    uint32_t tailerIndex;
    uint32_t unknown;
    uint32_t unknown2;
} sectionHeader_t;

typedef struct {
    uint8_t skip;
    uint8_t numVoxels;
} voxelSpanSegment_t;

typedef struct {
    int32_t *spanStart;
    int32_t *spanEnd; 
    voxelSpanSegment_t sections[];
} sectionData_t;

typedef struct {
    uint32_t spanStartOffset;
    uint32_t spanEndOffset;
    uint32_t spanDataOffset;
    float scale;
    float transform[3][4];
    float minBounds[3];
    float maxBounds[3]; 
    uint8_t xSize;
    uint8_t ySize;
    uint8_t zSize;
    uint8_t normalType;
} sectionTailer_t;

typedef struct {
    sectionHeader_t header;
    sectionTailer_t tailer;
    sectionData_t data;
} voxelSection_t;

//TODO: cleanup if possible
static void vxl_import(const char *path) {
    path = path ?: noc_file_dialog_open(NOC_FILE_DIALOG_OPEN, "vxl\0*.vxl\0",
                                        NULL, NULL);
    if (!path) return;
    header_t header;
    FILE *file;
    file = fopen(path, "rb");
    for(int i = 0; i<16 ; i++)
        header.fileType[i] = READ(char, file);

    if(strncmp(header.fileType, "Voxel Animation", 5))
        return;
    
    mat4_copy(mat4_zero, goxel.image->box);
    goxel.snap_mask |= SNAP_PLANE;

    header.paletteCount = READ(uint32_t, file);
    header.headerCount = READ(uint32_t, file);
    header.tailerCount = READ(uint32_t, file);
    header.bodySize = READ(uint32_t, file);
    
    vxl_palette_t palettes[header.paletteCount];
    for(int c = 0 ; c < header.paletteCount ; c++) {
        palettes[c].startPaletteRemap = READ(uint8_t, file);
        palettes[c].endPaletteRemap = READ(uint8_t, file);
        for(int x = 0 ; x < 256 ; x++) {
            for(int y = 0 ; y < 3 ; y++) {
                palettes[c].palette[x][y] = READ(uint8_t, file);
            }
        }
    }

    if(header.paletteCount >= 1) {
        palette_t *pal;
        pal = calloc(1, sizeof(*pal));
        pal->size = 256;
        snprintf(pal->name, sizeof(pal->name), "%s", path);
        snprintf(pal->name, sizeof(pal->name), "%s", sys_get_filename(pal->name));
        
        for(int x = 0 ; x < 255 ; x++) {
            for(int y = 0 ; y < 3 ; y++) {
                pal->entries[x].color[y] = palettes[header.paletteCount - 1].palette[x][y];
            }
            pal->entries[x].color[3] = 255;
        }
        
        DL_APPEND(goxel.palettes, pal);
        goxel.palette = pal;
    }

    voxelSection_t sections[header.headerCount];
    for(int i = 0 ; i < header.headerCount ; i++ ) {
        sectionHeader_t header;
        uint8_t character, terminated = 0;
        for(int i = 0; i<16 ; i++) {
            character = READ(char, file);
            
            if(character == 0x00) {
                terminated = 1;
                header.name[i] = 0;
            } else if(!terminated)
                header.name[i] = character;
        }
        header.tailerIndex = READ(uint32_t, file);
        header.unknown = READ(uint32_t, file);
        header.unknown2 = READ(uint32_t, file);
        sections[i].header = header;
    }
    
    uint32_t bodyStart = ftell(file);
    
    //Skip to tailer
    fseek(file, sizeof(header_t) + sizeof(vxl_palette_t) * header.paletteCount + sizeof(sectionHeader_t) * header.headerCount + header.bodySize, 0);
    
    sectionTailer_t tailers[header.tailerCount];
    for(int i = 0 ; i < header.tailerCount ; i++) {
        sectionTailer_t tailer;
        tailer.spanStartOffset = READ(uint32_t, file);
        tailer.spanEndOffset = READ(uint32_t, file);
        tailer.spanDataOffset = READ(uint32_t, file);
        tailer.scale = READ(float, file);
        for(int x = 0 ; x < 3 ; x++) {
            for(int y = 0 ; y < 4 ; y++) {
                tailer.transform[x][y] = READ(float, file);
            }        
        }

        for(int di = 0 ; di < 3 ; di++)
            tailer.minBounds[di] = READ(float, file);

        for(int di = 0 ; di < 3 ; di++)
            tailer.maxBounds[di] = READ(float, file);

        tailer.xSize = READ(uint8_t, file);
        tailer.ySize = READ(uint8_t, file);
        tailer.zSize = READ(uint8_t, file);
        tailer.normalType = READ(uint8_t, file);
        tailers[i] = tailer;
    }
    
    for(int i = 0 ; i < header.headerCount ; i++)
        sections[i].tailer = tailers[sections[i].header.tailerIndex];
    
    int pos[3];     
    uint8_t colour[2];
    fseek(file, bodyStart, 0);
    for (int i = 0; i < header.headerCount; i++) {
        layer_t *layer = image_add_layer(goxel.image, NULL);
        for(int u = 0 ; u < 16 ; u++)
            layer->name[u] = sections[i].header.name[u];
        sectionData_t data;
        fseek(file, bodyStart + sections[i].tailer.spanStartOffset, 0);
        long n = sections[i].tailer.xSize * sections[i].tailer.ySize;

        data.spanStart = (int32_t*) malloc (n*sizeof(int32_t));
        data.spanEnd = (int32_t*) malloc (n*sizeof(int32_t));

        for (int di = 0 ; di < n ; di++)
            data.spanStart[di] = READ(int32_t, file);

        for (int di = 0 ; di < n ; di++)
            data.spanEnd[di] = READ(int32_t, file);

        uint32_t dataStart = sections[i].tailer.spanDataOffset + bodyStart;

        for (int di = 0; di < n; di++)
        {
            if (data.spanStart[di] == -1)
                continue;
            
            fseek(file, dataStart + data.spanStart[di], 0);
            int x = (di % sections[i].tailer.xSize);
            int y = (di / sections[i].tailer.xSize);
            vec2_set(pos, x, y);
            int z = 0;
            while (z < sections[i].tailer.zSize) {
                z += READ(uint8_t, file);
                uint8_t count = READ(uint8_t, file);
                for (int j = 0; j < count; j++)
                {
                    uint8_t vColour = READ(uint8_t, file);
                    //TODO: do something with this
                    //uint8_t vNormal = READ(uint8_t, file);
                    //dump normals for now
                    READ(uint8_t, file);
                    pos[2] = z;
                    vec2_set(colour, vColour, 255);
                    z++;
                    mesh_set_at(layer->mesh, NULL, pos, colour);
                }

                READ(uint8_t, file);
            } 
            
            assert(ftell(file) - 1 == dataStart + data.spanEnd[di]);
        }
        int x = 0, y = 0, z = 0;
        int w, h, d;
        float p[3];
        w = sections[i].tailer.xSize;
        h = sections[i].tailer.ySize;
        d = sections[i].tailer.zSize;
        
        vec3_set(p, x + w / 2., y + h / 2., z + d / 2.);
        bbox_from_extents(layer->box, p, w / 2., h / 2., d / 2.);
        
        free(data.spanStart);
        free(data.spanEnd);
    }   
    image_delete_layer(goxel.image, &goxel.image->layers[0]);
}

static void export_as_vxl(const char *path) {
    path = path ?: noc_file_dialog_open(NOC_FILE_DIALOG_SAVE,
                    "Westwood vxl\0*.vxl\0", NULL, "untitled.vxl");
    if (!path) return;
    FILE *file;
    file = fopen(path, "wb");
    
    char *fileType = "Voxel Animation";
    for(int i = 0 ; i < 16; i++) {
        if (i < strlen(fileType))
            WRITE(char, fileType[i], file);
        else
            WRITE(char, 0, file);
    }
    // Number of palettes
    WRITE(uint32_t, 1, file);
    
    uint32_t numSections = 0;
    layer_t *layer;
    DL_FOREACH(goxel.image->layers, layer) {
        numSections++;
    }
    WRITE(uint32_t, numSections, file);
    WRITE(uint32_t, numSections, file);
    
    long bodySizePos = ftell(file);
    // Write empty for until body size is known
    WRITE(uint32_t, 0, file);
    
    WRITE(uint8_t, 16, file);
    WRITE(uint8_t, 31, file);
    for(int x = 0 ; x < 256 ; x++) {
        for(int y = 0 ; y < 3 ; y++) {
            WRITE(uint8_t, goxel.palette->entries[x].color[y], file);
        }
    }
    
    uint32_t i = 0;
    DL_FOREACH(goxel.image->layers, layer) {
        char *name = layer->name;
        for(int i = 0 ; i < 16; i++) {
            if (i < strlen(name))
                WRITE(char, name[i], file);
            else
                WRITE(char, 0, file);
        }
        WRITE(uint32_t, i++, file);
        WRITE(uint32_t, 1, file);
        WRITE(uint32_t, 2, file);
    }
    long headerEndOffset = ftell(file);
    
    i = 0;
    // Body
    int32_t *spanStart[numSections];
    int32_t *spanEnd[numSections];
    int32_t *spanLength[numSections];
    int32_t spanStartsOffset[numSections];
    int32_t spanEndsOffset[numSections];
    int32_t voxelsOffset[numSections];
    
    DL_FOREACH(goxel.image->layers, layer) {
        // Create bounding box for layer if it does not exist yet.
        if (box_is_null(layer->box)) {
            int32_t bbox[2][3];
            mesh_get_bbox(layer->mesh, bbox, true);
            if (bbox[0][0] > bbox[1][0]) memset(bbox, 0, sizeof(bbox));
            bbox_from_aabb(layer->box, bbox);
        }
        
        int32_t w = layer->box[0][0] * 2;
        int32_t h = layer->box[1][1] * 2;
        int32_t d = layer->box[2][2] * 2;
        int32_t xmin = 0, ymin = 0, zmin = 0;
        
        int32_t pos[3];
        uint8_t v[2];
        
        mesh_iterator_t iter = mesh_get_iterator(layer->mesh, MESH_ITER_VOXELS);
        while (mesh_iter(&iter, pos)) {
            mesh_get_at(layer->mesh, &iter, pos, v);
            if (v[1] < 127) continue;
            v[1] = 255;
            xmin = min(xmin, pos[0]);
            ymin = min(ymin, pos[1]);
            zmin = min(zmin, pos[2]);
        }
        
        uint8_t voxels[w*h][d][2];
        
        spanStart[i] = (int32_t*) malloc((w*h)*sizeof(int32_t));
        spanEnd[i] = (int32_t*) malloc((w*h)*sizeof(int32_t));
        spanLength[i] = (int32_t*) malloc((w*h)*sizeof(int32_t));
        uint8_t c_voxel[2];
        int32_t c_offset = 0;
        for(int32_t xy = 0; xy < w*h; xy++) {
            pos[0] = (xy % w) + xmin;
            pos[1] = xy / w + ymin;
            
            uint8_t v_count = 0, skip = 0;
            int32_t c_length = 0;
            
            for(int32_t z = 0; z < d; z++) {
                pos[2] = z + zmin;
                mesh_get_at(layer->mesh, NULL, pos, c_voxel);
                // Somehow mesh_get_at is broken with the voxel changes??
                voxels[xy][z][0] = c_voxel[0];
                voxels[xy][z][1] = c_voxel[1];
            
                if(c_voxel[1] < 127) {
                    if(v_count <= 0) {
                        skip++;
                    // At the end.
                    } else if (v_count > 0) {
                        // Start new segment
                        c_length += sizeof(uint8_t) * 3;
                        v_count = 0;
                        skip = 1;
                    }
                    
                    if(z == d - 1)
                        c_length += sizeof(uint8_t) * 3;
                } else {
                    v_count++;
                    c_length += sizeof(uint8_t) * 2;
                    if(z == d - 1) {
                        c_length += sizeof(uint8_t) * 3;
                    }
                }
            }
            
            if(skip >= d) {
                spanStart[i][xy] = -1;
                spanEnd[i][xy] = -1;
            } else {
                spanLength[i][xy] = c_length;
                spanStart[i][xy] = c_offset;
                c_offset += c_length;
                spanEnd[i][xy] = c_offset - 1;
            }
        }
        
        spanStartsOffset[i] = ftell(file);

        for(int32_t xy = 0 ; xy < w*h; xy++)
            WRITE(int32_t, spanStart[i][xy], file);
        
        spanEndsOffset[i] = ftell(file);
        
        for(int32_t xy = 0 ; xy < w*h; xy++)
            WRITE(int32_t, spanEnd[i][xy], file);
        
        voxelsOffset[i] = ftell(file);
        
        for(int32_t xy = 0 ; xy < w*h; xy++) {
            if(spanStart[i][xy] == -1)
                continue;
            uint8_t write_buffer[spanLength[i][xy] / sizeof(uint8_t)];
            
            write_buffer[0] = 0;
            write_buffer[1] = d;
            write_buffer[spanLength[i][xy] / sizeof(uint8_t) - 1] = d;
            int32_t v_count_ind = 1, skip_ind = 0, current_ind = 1;
            uint8_t skip = 0, v_count = 0;
            for(int32_t z = 0; z < d; z++) {
                c_voxel[0] = voxels[xy][z][0];
                c_voxel[1] = voxels[xy][z][1];
                
                if(c_voxel[1] < 127) {
                    if(v_count <= 0) {
                        skip++;
                        // At the end.
                    } else if (v_count > 0) {
                        // Start new segment
                        write_buffer[skip_ind] = skip;
                        write_buffer[v_count_ind] = v_count;
                        current_ind++;
                        write_buffer[current_ind] = v_count;
                        skip_ind = ++current_ind;
                        v_count_ind = ++current_ind;
                        v_count = 0;
                        skip = 1;
                    }
                    
                    if(z == d - 1) {
                        write_buffer[v_count_ind] = 0;
                        write_buffer[skip_ind] = skip;
                        write_buffer[++current_ind] = 0;
                    }
                } else {
                    v_count++;
                    current_ind++;
                    write_buffer[current_ind] = c_voxel[0];
                    
                    current_ind++;
                    write_buffer[current_ind] = 0;
                    
                    if(z == d - 1) {
                        write_buffer[v_count_ind] = v_count;
                        write_buffer[skip_ind] = skip;
                        write_buffer[++current_ind] = v_count;
                    }
                }
            }
            
            for(int wb = 0; wb < spanLength[i][xy] / sizeof(uint8_t); wb++)
                WRITE(uint8_t, write_buffer[wb], file);
        }
        
        free(spanStart[i]);
        free(spanEnd[i]);
        free(spanLength[i]);
        i++;
    }
    long bodyEndOffset = ftell(file);
    
    i = 0;
    // Tailer
    DL_FOREACH(goxel.image->layers, layer) {
        int32_t w = layer->box[0][0] * 2;
        int32_t h = layer->box[1][1] * 2;
        int32_t d = layer->box[2][2] * 2;

        WRITE(uint32_t, spanStartsOffset[i] - headerEndOffset, file);
        WRITE(uint32_t, spanEndsOffset[i] - headerEndOffset, file);
        WRITE(uint32_t, voxelsOffset[i] - headerEndOffset, file);

        //TODO: Make these layer specific.
        WRITE(float, 0.0833333358f, file);
        float transform[3][4] = {
            {1,0,0,0},
            {0,1,0,0},
            {0,0,1,0}
        };

        for(int x = 0 ; x < 3 ; x++) {
            for(int y = 0 ; y < 4 ; y++) {
                WRITE(float, transform[x][y], file);
            }
        }
        
        WRITE(float, -layer->box[0][0], file);
        WRITE(float, -layer->box[1][1], file);
        //TODO: figure out if this can change.
        WRITE(float, 0, file);

        WRITE(float, layer->box[0][0], file);
        WRITE(float, layer->box[1][1], file);
        WRITE(float, d, file);

        WRITE(uint8_t, w, file);
        WRITE(uint8_t, h, file);
        WRITE(uint8_t, d, file);

        WRITE(uint8_t, goxel.normalType, file);

        i++;
    }
    
    fseek(file, bodySizePos, 0);
    WRITE(uint32_t, bodyEndOffset - headerEndOffset, file);
    
    fclose(file);
}

static void action_open(const char *path)
{

    if (!path)
        path = noc_file_dialog_open(NOC_FILE_DIALOG_OPEN, "vxl\0*.vxl\0",
                                    NULL, NULL);
    if (!path) return;
    image_delete(goxel.image);
    goxel.image = image_new();
    vxl_import(path);
}

ACTION_REGISTER(open,
    .help = "Open an image",
    .cfunc = action_open,
    .csig = "vp",
    .default_shortcut = "Ctrl O",
)

static void save_as(const char *path)
{
    if (!path) {
        path = noc_file_dialog_open(NOC_FILE_DIALOG_SAVE, "vxl\0*.vxl\0",
                                    NULL, "untitled.vxl");
        if (!path) return;
    }
    if (path != goxel.image->path) {
        free(goxel.image->path);
        goxel.image->path = strdup(path);
        goxel.image->saved_key = image_get_key(goxel.image);
    }
    export_as_vxl(goxel.image->path);
}

ACTION_REGISTER(save_as,
    .help = "Save the image as",
    .cfunc = save_as,
    .csig = "vpi",
//     .default_shortcut = "Ctrl Shift S"
)

static void save(const char *path)
{
    save_as(path ?: goxel.image->path);
}

ACTION_REGISTER(save,
    .help = "Save the image",
    .cfunc = save,
    .csig = "vpi",
    .default_shortcut = "Ctrl S"
)


ACTION_REGISTER(import_vxl,
    .help = "Import a Westwood voxel file",
    .cfunc = vxl_import,
    .csig = "vp",
    .file_format = {
        .name = "vxl",
        .ext = "*.vxl\0"
    }, 
)

ACTION_REGISTER(export_as_vxl,
    .help = "Export the image as a Westwood voxel file",
    .cfunc = export_as_vxl,
    .csig = "vp",
    .file_format = {
        .name = "vxl",
        .ext = "*.vxl\0",
    },
)
